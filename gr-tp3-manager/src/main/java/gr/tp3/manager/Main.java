package gr.tp3.manager;

import gr.tp3.manager.view.MainMenuView;
import gr.tp3.manager.controller.EventController;
import gr.tp3.manager.domain.services.EventFilterService;
import gr.tp3.manager.persistence.SNMPEventRepository;
import gr.tp3.manager.snmp.SNMPConfigurationImportService;
import gr.tp3.manager.snmp.SNMPRequestService;

public class Main {

    public static void main(String[] args) throws Exception {

        SNMPConfigurationImportService snmpConfigurationImportService = new SNMPConfigurationImportService();
        SNMPRequestService snmpRequestService = new SNMPRequestService(snmpConfigurationImportService);
        SNMPEventRepository snmpEventRepository = new SNMPEventRepository(snmpRequestService);
        snmpEventRepository.start();

        EventFilterService eventFilterService = new EventFilterService(snmpEventRepository);
        EventController eventController = new EventController(eventFilterService);

        MainMenuView mainMenuView = new MainMenuView(eventController);
        mainMenuView.mainPage();
        snmpEventRepository.stopThread();
        snmpEventRepository.interrupt();

    }
}
