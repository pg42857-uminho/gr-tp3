package gr.tp3.manager.controller;

import gr.tp3.manager.dto.EventViewDTO;
import gr.tp3.manager.domain.services.EventFilterService;

import java.util.List;

public class EventController {

    private EventFilterService eventFilterService;

    public EventController(EventFilterService eventFilterService) {
        this.eventFilterService = eventFilterService;
    }

    public List<EventViewDTO> retrievePreviousEvents(){
        return eventFilterService.retrievePreviousEvents();
    }

    public List<EventViewDTO> retrieveCurrentEvents(){
        return eventFilterService.retrieveCurrentEvents();
    }

    public List<EventViewDTO> retrieveFutureEvents(){
        return eventFilterService.retrieveFutureEvents();
    }

    public List<EventViewDTO> retrieveAllEvents() {
        return eventFilterService.retrieveAll();
    }

}
