package gr.tp3.manager.domain.event;

import gr.tp3.manager.dto.EventPersistenceDTO;

import java.util.*;

public class EventFactory {

    private Optional<Event> createEvent(EventPersistenceDTO eventPersistenceDTO) {
        String index = eventPersistenceDTO.index;
        String description = eventPersistenceDTO.description;
        String timeToString = eventPersistenceDTO.timeToString;
        String status = eventPersistenceDTO.status;

        try {
            Event event = new Event(index, description, timeToString, status);
            return Optional.of(event);
        }
        catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    public Map<String, Event> createEventMap(Collection<EventPersistenceDTO> dtoCollection) {
        Map<String, Event> eventList = new HashMap<>();

        for (EventPersistenceDTO dto: dtoCollection) {
            Optional<Event> eventOptional = createEvent(dto);

            if (eventOptional.isPresent()) {
                Event event = eventOptional.get();
                String index = event.getIndex();

                eventList.put(index, event);
            }
        }

        return eventList;
    }

}
