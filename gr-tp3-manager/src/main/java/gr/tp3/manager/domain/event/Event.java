package gr.tp3.manager.domain.event;

import gr.tp3.manager.dto.EventViewDTO;

import java.util.Objects;
import java.util.regex.Pattern;

public class Event {

    private static final Pattern TIMETOSTRING_PATTERN =
            Pattern.compile("^[+-]?\\d* years [+-]?\\d{2} months [+-]?\\d{2} weeks [+-]?\\d{2} days [+-]?\\d{2} hours [+-]?\\d{2} minutes$");

    private static final Pattern TIMETOSTRING_NEGATIVE_PATTERN =
            Pattern.compile("^-\\d* years -\\d{2} months -\\d{2} weeks -\\d{2} days -\\d{2} hours -\\d{2} minutes$");

    private static final Pattern TIMETOSTRING_POSITIVE_PATTERN =
            Pattern.compile("^[+]\\d* years [+]\\d{2} months [+]\\d{2} weeks [+]\\d{2} days [+]\\d{2} hours [+]\\d{2} minutes$");

    private static final String TIMETOSTRING_ZERO = "+00 years +00 months +00 weeks +00 days +00 hours +00 minutes";

    private String index;
    private String description;
    private String timeToString;
    private String status;

    public Event(String index,
                 String description,
                 String timeToString,
                 String status) {
        setIndex(index);
        setDescription(description);
        setTimeToString(timeToString);
        setStatus(status);
    }

    /**
     * Clone constructor
     * @param event
     */
    public Event (Event event) {
        this(event.index, event.description, event.timeToString, event.status);
    }

    protected String getIndex() {
        return index;
    }

    /**
     * If the event is still to occur => 1
     * If the event is happening => 0
     * If the event has already happened => -1
     *
     * @return 1, 0 or -1
     */
    public int getTimeToStatus() {
        if (TIMETOSTRING_ZERO.compareTo(timeToString) == 0) {
            return 0;
        }
        else if (TIMETOSTRING_POSITIVE_PATTERN.matcher(timeToString).matches()) {
            return 1;
        }
        else {
            return -1;
        }
    }

    private void setIndex(String index) {
        if (index == null || index.trim().isEmpty()) {
            throw new IllegalArgumentException("Index cant be null nor empty!");
        }

        try {
            Integer.parseInt(index);
        } catch (Exception e) {
            throw new IllegalArgumentException("Index must be a number!");
        }

        this.index = index;
    }

    private void setDescription(String description) {
        if (description == null || description.trim().isEmpty()) {
            throw new IllegalArgumentException("Description cant be null nor empty!");
        }

        this.description = description;
    }

    private void setTimeToString(String timeToString) {
        if (timeToString == null || timeToString.trim().isEmpty()) {
            throw new IllegalArgumentException("TimeToString cant be null nor empty!");
        }

        if(!TIMETOSTRING_PATTERN.matcher(timeToString).matches()) {
            throw new IllegalArgumentException("TimeToString must match the defined pattern!");
        }

        this.timeToString = timeToString;
    }

    private void setStatus(String status) {
        if (status == null || status.trim().isEmpty()) {
            throw new IllegalArgumentException("Status cant be null nor empty!");
        }

        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return  Objects.equals(index, event.index) &&
                Objects.equals(description, event.description) &&
                Objects.equals(timeToString, event.timeToString) &&
                Objects.equals(status, event.status);
    }

    public EventViewDTO toEventViewDTO() {
        EventViewDTO eventViewDTO = new EventViewDTO(index, description, timeToString, status);
        return eventViewDTO;
    }

    @Override
    public int hashCode() {
        return Objects.hash(index, description, timeToString, status);
    }

    @Override
    public String toString() {
        return "snmp.manager.domain.model.Event{" +
                "index='" + index + '\'' +
                ", description='" + description + '\'' +
                ", timeTo='" + timeToString + '\'' +
                ", status='" + status +
                '}';
    }
}
