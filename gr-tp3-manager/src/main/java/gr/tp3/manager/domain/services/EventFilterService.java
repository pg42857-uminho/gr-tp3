package gr.tp3.manager.domain.services;

import gr.tp3.manager.domain.event.Event;
import gr.tp3.manager.domain.event.EventRepository;
import gr.tp3.manager.dto.EventViewDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class EventFilterService {

    private EventRepository eventRepository;

    public EventFilterService (EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public List<EventViewDTO> retrievePreviousEvents(){
        Predicate<Event> filter = e -> e.getTimeToStatus() < 0;

        List<Event> eventList = eventRepository.retrieveAllThatMatch(filter);
        return parseEvents(eventList);
    }

    public List<EventViewDTO> retrieveCurrentEvents(){
        Predicate<Event> filter = e -> e.getTimeToStatus() == 0;

        List<Event> eventList = eventRepository.retrieveAllThatMatch(filter);
        return parseEvents(eventList);
    }

    public List<EventViewDTO> retrieveFutureEvents(){
        Predicate<Event> filter = e -> e.getTimeToStatus() > 0;

        List<Event> eventList = eventRepository.retrieveAllThatMatch(filter);
        return parseEvents(eventList);
    }

    public List<EventViewDTO> retrieveAll() {
        List<Event> eventList = eventRepository.retrieveAll();
        return parseEvents(eventList);
    }

    private List<EventViewDTO> parseEvents(List<Event> eventList) {
        List<EventViewDTO> dtoList = new ArrayList<>();

        for (Event event : eventList) {
            EventViewDTO eventViewDTO = event.toEventViewDTO();
            dtoList.add(eventViewDTO);
        }

        return dtoList;
    }
}
