package gr.tp3.manager.domain.event;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public interface EventRepository {

    Optional<Event> retrieve(String index);

    List<Event> retrieveAll();

    List<Event> retrieveAllThatMatch(Predicate<Event> filter);
}
