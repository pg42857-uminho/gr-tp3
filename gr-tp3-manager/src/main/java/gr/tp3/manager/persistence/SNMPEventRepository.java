package gr.tp3.manager.persistence;

import gr.tp3.manager.domain.event.Event;
import gr.tp3.manager.domain.event.EventFactory;
import gr.tp3.manager.domain.event.EventRepository;
import gr.tp3.manager.dto.EventPersistenceDTO;
import gr.tp3.manager.snmp.SNMPRequestService;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class SNMPEventRepository extends Thread implements EventRepository {

    /**
     * Updates database every 60 seconds.
     */
    private static final long UPDATE_FREQUENCY = 60000;

    private SNMPRequestService snmpRequestService;
    private volatile Map<String, Event> events;

    private boolean running = true;

    public SNMPEventRepository(SNMPRequestService snmpRequestService) {
        this.snmpRequestService = snmpRequestService;
        this.events = new HashMap<>();
    }

    @Override
    public Optional<Event> retrieve(String index) {
        if (events.containsKey(index)) {
            Event event = events.get(index);
            return Optional.of(new Event(event));
        }
        else {
            return Optional.empty();
        }
    }

    @Override
    public List<Event> retrieveAll() {
        return cloneEvents(new ArrayList<>(this.events.values()));
    }

    @Override
    public List<Event> retrieveAllThatMatch(Predicate<Event> filter) {
        return cloneEvents(this.events.values().stream()
                        .filter(filter)
                        .collect(Collectors.toList()));
    }

    @Override
    public void run() {
        while (running) {
            long now = System.currentTimeMillis();

            load();

            try {
                Thread.sleep((now + UPDATE_FREQUENCY) - System.currentTimeMillis());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void stopThread() {
        this.running = false;
    }

    private List<Event> cloneEvents(List<Event> eventList) {
        List<Event> clonedEventList = new ArrayList<>();

        for(Event event : eventList) {
            clonedEventList.add(new Event(event));
        }

        return clonedEventList;
    }

    private void load() {
        Map<String, EventPersistenceDTO> eventDTOMap = snmpRequestService.requestEvents();

        EventFactory eventFactory = new EventFactory();
        Map<String, Event> eventMap = eventFactory.createEventMap(eventDTOMap.values());

        this.events = eventMap;
    }

}
