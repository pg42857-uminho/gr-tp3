package gr.tp3.manager.snmp;

import gr.tp3.manager.dto.EventPersistenceDTO;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.util.TreeEvent;

import java.util.*;

public class SNMPRequestService {

    private SNMPConfigurationImportService SNMPConfigurationImportService;

    public SNMPRequestService(SNMPConfigurationImportService configurationImportService) {
        this.SNMPConfigurationImportService = configurationImportService;
    }

    public Map<String, EventPersistenceDTO> requestEvents() {
        SNMPConfiguration theSNMPConfiguration = SNMPConfigurationImportService.snmpObjectReader();
        SNMPClient theRequest = new SNMPClient();

        Map<String, EventPersistenceDTO> eventDTOMap = new HashMap<>();

        try {
            var theList = theRequest.snmpRequest(theSNMPConfiguration);

            for (TreeEvent theTreeEvent : theList) {
                for (VariableBinding event : theTreeEvent.getVariableBindings()) {

                    String oid = event.getOid().toString();
                    String iodIndex = oid.substring(oid.lastIndexOf(".")).replace(".","");
                    String iodProcess = oid.substring(oid.lastIndexOf(".") - 2, oid.lastIndexOf(".")).replace(".","");
                    String vari = event.getVariable().toString();

                    if (iodProcess.equals("1")) {
                        EventPersistenceDTO eventPersistenceDTO = new EventPersistenceDTO();
                        eventPersistenceDTO.index = iodIndex;

                        eventDTOMap.put(iodIndex, eventPersistenceDTO);
                    }
                    else if (iodProcess.equals("2") && eventDTOMap.containsKey(iodIndex)) {
                        eventDTOMap.get(iodIndex).description = vari;
                    }
                    else if (iodProcess.equals("3") && eventDTOMap.containsKey(iodIndex)) {
                        eventDTOMap.get(iodIndex).timeToString = vari;
                    }
                    else if (iodProcess.equals("4") && eventDTOMap.containsKey(iodIndex)) {
                        eventDTOMap.get(iodIndex).status = vari;
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Host Unreachable...");
        }

        return eventDTOMap;
    }

}
