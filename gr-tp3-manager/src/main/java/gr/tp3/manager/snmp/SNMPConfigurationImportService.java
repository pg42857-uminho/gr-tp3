package gr.tp3.manager.snmp;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;

public class SNMPConfigurationImportService {

    public static final String CONFIGURATION_FILE = "./configuration.json";
    public static final String IPADDRESS_PROPERTY = "ip_address";
    public static final String PORT_PROPERTY = "port";
    public static final String COMMSTRING_PROPERTY = "community_string";

    public SNMPConfiguration snmpObjectReader() {
        SNMPConfiguration snmpConfiguration = getSNMPConfiguration(CONFIGURATION_FILE);

        return snmpConfiguration;
    }

    private SNMPConfiguration getSNMPConfiguration(String path) {
        JSONParser parser = new JSONParser();
        try {
            JSONObject jsonObject = (JSONObject) parser.parse(new FileReader(path));

            String ipAddress = (String) jsonObject.get(IPADDRESS_PROPERTY);
            long portNumber = (long) jsonObject.get(PORT_PROPERTY);
            String communityString = (String) jsonObject.get(COMMSTRING_PROPERTY);

            SNMPConfiguration snmpConfiguration =
                    new SNMPConfiguration(ipAddress, portNumber, communityString);

            return snmpConfiguration;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return SNMPConfiguration.getDefaultInstance();
    }

}
