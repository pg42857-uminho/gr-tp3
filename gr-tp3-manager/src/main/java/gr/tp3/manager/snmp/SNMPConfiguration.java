package gr.tp3.manager.snmp;

import java.util.regex.Pattern;

public class SNMPConfiguration {

    private static final Pattern OID_PATTERN = Pattern.compile("[[0-9]+\\.]*");
    /**
     * https://www.freeformatter.com/java-regex-tester.html#ad-output
     */
    private static final Pattern IPADDRESS_PATTERN =
            Pattern.compile("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");

    private static final String IPADDRESS_DEFAULT = "127.0.0.1";
    private static final int PORTNUMBER_DEFAULT = 3003;
    private static final String COMMUNITYSTRING_DEFAULT = "public";
    private static final String OID_DEFAULT = "1.3.6.1.3.20210130.1.1";

    private String ipAddress;
    private long portNumber;
    private String communityString;
    private String oid;

    public SNMPConfiguration(String ipAddress, long portNumber, String communityString) {
        setIpAddress(ipAddress);
        setPortNumber(portNumber);
        setCommunityString(communityString);
        setOid(OID_DEFAULT);
    }

    public String ipAddress() {
        return ipAddress;
    }

    public long portNumber() {
        return portNumber;
    }

    public String communityString() {
        return communityString;
    }

    public String oid() {
        return oid;
    }

    private void setIpAddress(String ipAddress) {
        if (ipAddress == null || ipAddress.trim().isEmpty()) {
            throw new IllegalArgumentException("IP Address must not be null nor empty!");
        }

        if(!IPADDRESS_PATTERN.matcher(ipAddress).matches()) {
            throw new IllegalArgumentException("IP Address must match the pattern!");
        }

        this.ipAddress = ipAddress;
    }

    private void setPortNumber(long portNumber) {
        if(portNumber < 0 || portNumber > 65535) {
            throw new IllegalArgumentException("PortNumber must be from 0 to 65535!");
        }

        this.portNumber = portNumber;
    }

    private void setCommunityString(String communityString) {
        if(communityString == null) {
            throw new IllegalArgumentException("CommunityString must not be null!");
        }

        this.communityString = communityString;
    }

    private void setOid(String oid) {
        if(oid == null || oid.trim().isEmpty()) {
            throw new IllegalArgumentException("OID must not be null nor empty!");
        }

        if(!OID_PATTERN.matcher(oid).matches()) {
            throw new IllegalArgumentException("OID must match the OID pattern!");
        }

        this.oid = oid;
    }

    @Override
    public String toString() {
        return "snmp.manager.snmp.SNMPObject{" +
                "host_IP='" + ipAddress + '\'' +
                ", port='" + portNumber + '\'' +
                ", comm_string='" + communityString + '\'' +
                ", oID='" + oid + '\'' +
                '}';
    }

    protected static SNMPConfiguration getDefaultInstance() {
        return new SNMPConfiguration(
                IPADDRESS_DEFAULT,
                PORTNUMBER_DEFAULT,
                COMMUNITYSTRING_DEFAULT);
    }
}
