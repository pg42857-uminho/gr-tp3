package gr.tp3.manager.snmp;

import org.snmp4j.CommunityTarget;
import org.snmp4j.Snmp;
import org.snmp4j.Target;
import org.snmp4j.TransportMapping;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.DefaultPDUFactory;
import org.snmp4j.util.TreeEvent;
import org.snmp4j.util.TreeUtils;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @see "https://examples.javacodegeeks.com/enterprise-java/snmp4j/snmp-walk-example-using-snmp4j/"
 */
public class SNMPClient {

    protected SNMPClient(){}

    protected List<TreeEvent> snmpRequest(SNMPConfiguration snmpConfiguration) throws Exception {
        CommunityTarget target = new CommunityTarget();
        target.setCommunity(new OctetString(snmpConfiguration.communityString()));
        String address = "udp:"+ snmpConfiguration.ipAddress()+"/"+ snmpConfiguration.portNumber();
        target.setAddress(GenericAddress.parse(address));
        target.setRetries(2);
        target.setTimeout(1500);
        target.setVersion(SnmpConstants.version2c);
        return doWalk(snmpConfiguration.oid(), target);
    }

    private List<TreeEvent> doWalk(String tableOid, Target target) throws IOException {
        Map<String, String> result = new TreeMap<>();
        TransportMapping<? extends Address> transport = new DefaultUdpTransportMapping();
        Snmp snmp = new Snmp(transport);
        transport.listen();

        TreeUtils treeUtils = new TreeUtils(snmp, new DefaultPDUFactory());
        List<TreeEvent> tree = treeUtils.getSubtree(target, new OID(tableOid));
        snmp.close();
        return tree;
    }
}
