package gr.tp3.manager.view;

import gr.tp3.manager.controller.EventController;
import gr.tp3.manager.dto.EventViewDTO;

import java.util.Formatter;
import java.util.List;
import java.util.Scanner;

public class MainMenuView {

    private Formatter out = new Formatter(System.out);
    private Scanner in = new Scanner(System.in);

    private EventController eventController;

    public MainMenuView(EventController eventController) {
        this.eventController = eventController;
    }

    public void mainPage() {
        out.format("%n%s%n", getMenuHeader("      GR MANAGER 2020/21     "));

        String op;
        do {
            op = startupMenu();
            switch (op) {
                case "1":
                    List<EventViewDTO> futureEvents = eventController.retrieveFutureEvents();
                    displayEvents(futureEvents);
                    break;
                case "2":
                    List<EventViewDTO> currentEvents = eventController.retrieveCurrentEvents();
                    displayEvents(currentEvents);
                    break;
                case "3":
                    List<EventViewDTO> pastEvents = eventController.retrievePreviousEvents();
                    displayEvents(pastEvents);
                    break;
                case "4":
                    List<EventViewDTO> allEvents = eventController.retrieveAllEvents();
                    displayEvents(allEvents);
                    break;
                case "0":
                    break;
                default:
                    //OUT.format("%n%s", "Invalid option!");
                    break;
            }
        } while (!op.equals("0"));

        out.format("%n%s%n", "The application is closing!");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    private void displayEvents(List<EventViewDTO> events){
        CommandLineTable st = new CommandLineTable();
        st.setShowVerticalLines(true);
        st.setHeaders("Index", "Description", "TimeTo", "Status");

        for(EventViewDTO event : events){
            st.addRow(event.index, event.description, event.timeToString, event.status);
        }
        st.print();
    }

    /**
     * Start up menu
     *
     * @return option
     */
    private String startupMenu() {

        out.format("%n%s%n%s%n%s%n%s%n%n%s%n",
                "1 - Get future events.",
                "2 - Get current events.",
                "3 - Get past events.",
                "4 - Get all events.",
                "0 - Exit.");
        return in.nextLine().trim();
    }

    public String getMenuHeader(String menuHeader){
        int n = menuHeader.length();

        StringBuilder header = new StringBuilder();
        for (int i = 0; i < n+4; i++) {
            header.append("#");
        }
        header.append("\n").append("#");
        for (int i = 0; i < n+2; i++) {
            header.append(" ");
        }
        header.append("#");
        header.append("\n").append("# ").append(menuHeader).append(" #");
        header.append("\n").append("#");
        for (int i = 0; i < n+2; i++) {
            header.append(" ");
        }
        header.append("#").append("\n");
        for (int i = 0; i < n+4; i++) {
            header.append("#");
        }
        return header.toString();
    }

}
