package gr.tp3.manager.dto;

public class EventViewDTO {

    public String index;
    public String description;
    public String timeToString;
    public String status;

    public EventViewDTO(String index, String description, String timeToString, String status) {
        this.index = index;
        this.description = description;
        this.timeToString = timeToString;
        this.status = status;
    }
}
