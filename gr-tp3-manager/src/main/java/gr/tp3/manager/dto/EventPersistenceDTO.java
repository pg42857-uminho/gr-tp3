package gr.tp3.manager.dto;

public class EventPersistenceDTO {

    public String index;
    public String description;
    public String timeToString;
    public String status;
}
