package gr.tp3.manager.domain.event;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EventTest {

    private final String index = "1";
    private final String description = "Evento 1";
    private final String status = "Status";
    private final String timeToStringNegative = "-00 years -00 months -01 weeks -00 days -00 hours -13 minutes";
    private final String timeToStringZero = "+00 years +00 months +00 weeks +00 days +00 hours +00 minutes";
    private final String timeToStringPositive = "+02 years +10 months +02 weeks +07 days +02 hours +34 minutes";
    private final String timeToString = timeToStringPositive;

    @Test
    public void ensureIndexCantBeNull() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Event(null, description, timeToString, status);
        });
    }

    @Test
    public void ensureIndexCantBeEmpty() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Event("", description, timeToString, status);
        });
    }

    @Test
    public void ensureIndexCantContainWord() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Event("word", description, timeToString, status);
        });
    }

    @Test
    public void ensureDescriptionCantBeNull() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Event(index, null, timeToString, status);
        });
    }

    @Test
    public void ensureDescriptionCantBeEmpty() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Event(index, "", timeToString, status);
        });
    }

    @Test
    public void ensureTimeToStringCantBeNull() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Event(index, description, null, status);
        });
    }

    @Test
    public void ensureTimeToStringCantBeEmpty() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Event(index, description, "", status);
        });
    }

    @Test
    public void ensureTimeToStringCantBeWord() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Event(index, description, "word", status);
        });
    }

    @Test
    public void ensureStatusCantBeNull() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Event(index, description, timeToString, null);
        });
    }

    @Test
    public void ensureStatusCantBeEmpty() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Event(index, description, timeToString, "");
        });
    }

    @Test
    public void ensureEventGetsConstructed() {
        new Event(index, description, timeToString, status);
    }

    @Test
    public void ensureEventGetsConstructedWithPositiveTimeToString() {
        new Event(index, description, timeToStringPositive, status);
    }

    @Test
    public void ensureEventGetsConstructedWithNegativeTimeToString() {
        new Event(index, description, timeToStringNegative, status);
    }

    @Test
    public void ensureEventGetsConstructedWithZeroedTimeToString() {
        new Event(index, description, timeToStringZero, status);
    }

}