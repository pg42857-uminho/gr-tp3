/** Copyright Text */
package gr.tp3.agent;
// --AgentGen BEGIN=_BEGIN
// --AgentGen END

import gr.tp3.agent.greventsmanager.GrEventsSnmpMibManager;
import org.snmp4j.smi.*;
import org.snmp4j.agent.*;
import org.snmp4j.agent.mo.*;
import org.snmp4j.log.LogFactory;
import org.snmp4j.log.LogAdapter;

// --AgentGen BEGIN=_IMPORT
// --AgentGen END

public class GrEventsSnmpMib
        // --AgentGen BEGIN=_EXTENDS
        // --AgentGen END
        implements MOGroup
// --AgentGen BEGIN=_IMPLEMENTS
// --AgentGen END
{

    private static final LogAdapter LOGGER = LogFactory.getLogger(GrEventsSnmpMib.class);

    // --AgentGen BEGIN=_STATIC
    // --AgentGen END

    // Factory
    private MOFactory moFactory = DefaultMOFactory.getInstance();

    // Constants

    /** OID of this MIB module for usage which can be used for its identification. */
    public static final OID oidGrEventsSnmpMib = new OID(new int[] {1, 3, 6, 1, 3, 20210130});

    // Identities
    // Scalars
    public static final OID oidGrEventsNumber =
            new OID(new int[] {1, 3, 6, 1, 3, 20210130, 1, 2, 0});
    // Tables

    // Notifications

    // Enumerations

    // TextualConventions

    // Scalars
    private MOScalar<Integer32> grEventsNumber;

    // Tables
    public static final OID oidGrEventsEntry =
            new OID(new int[] {1, 3, 6, 1, 3, 20210130, 1, 1, 1});

    // Index OID definitions
    public static final OID oidGrEventsEntryIndex =
            new OID(new int[] {1, 3, 6, 1, 3, 20210130, 1, 1, 1, 1});

    // Column TC definitions for grEventsEntry:

    // Column sub-identifier definitions for grEventsEntry:
    public static final int colGrEventsEntryIndex = 1;
    public static final int colGrEventsEntryDescription = 2;
    public static final int colGrEventsEntryTimeto = 3;
    public static final int colGrEventsEntryStatus = 4;

    // Column index definitions for grEventsEntry:
    public static final int idxGrEventsEntryIndex = 0;
    public static final int idxGrEventsEntryDescription = 1;
    public static final int idxGrEventsEntryTimeto = 2;
    public static final int idxGrEventsEntryStatus = 3;

    private MOTableSubIndex[] grEventsEntryIndexes;
    private MOTableIndex grEventsEntryIndex;

    @SuppressWarnings(value = {"rawtypes"})
    private MOTable<GrEventsEntryRow, MOColumn, MOTableModel<GrEventsEntryRow>> grEventsEntry;

    private MOTableModel<GrEventsEntryRow> grEventsEntryModel;

    private GrEventsSnmpMibManager grEventsSnmpMibManager;

    // --AgentGen BEGIN=_MEMBERS
    // --AgentGen END

    /**
     * Constructs a GrEventsSnmpMib instance without actually creating its <code>ManagedObject
     * </code> instances. This has to be done in a sub-class constructor or after construction by
     * calling {@link #createMO(MOFactory moFactory)}.
     */
    protected GrEventsSnmpMib() {
        // --AgentGen BEGIN=_DEFAULTCONSTRUCTOR
        // --AgentGen END
        grEventsSnmpMibManager = new GrEventsSnmpMibManager(this);
    }

    /**
     * Constructs a GrEventsSnmpMib instance and actually creates its <code>ManagedObject</code>
     * instances using the supplied <code>MOFactory</code> (by calling {@link #createMO(MOFactory
     * moFactory)}).
     *
     * @param moFactory the <code>MOFactory</code> to be used to create the managed objects for this
     *     module.
     */
    public GrEventsSnmpMib(MOFactory moFactory) {
        this();
        // --AgentGen BEGIN=_FACTORYCONSTRUCTOR::factoryWrapper
        // --AgentGen END
        this.moFactory = moFactory;
        createMO(moFactory);
        // --AgentGen BEGIN=_FACTORYCONSTRUCTOR
        // --AgentGen END
    }

    // --AgentGen BEGIN=_CONSTRUCTORS
    // --AgentGen END

    /**
     * Create the ManagedObjects defined for this MIB module using the specified {@link MOFactory}.
     *
     * @param moFactory the <code>MOFactory</code> instance to use for object creation.
     */
    protected void createMO(MOFactory moFactory) {
        addTCsToFactory(moFactory);
        grEventsNumber =
                moFactory.createScalar(
                        oidGrEventsNumber,
                        moFactory.createAccess(MOAccessImpl.ACCESSIBLE_FOR_READ_ONLY),
                        new Integer32());
        createGrEventsEntry(moFactory);
    }

    public void loadManager(){
        grEventsSnmpMibManager.start();
    }

    public MOScalar<Integer32> getGrEventsNumber() {
        return grEventsNumber;
    }

    public void setGrEventsNumber(Integer32 value) {
        this.grEventsNumber.setValue(value);
    }

    @SuppressWarnings(value = {"rawtypes"})
    public MOTable<GrEventsEntryRow, MOColumn, MOTableModel<GrEventsEntryRow>> getGrEventsEntry() {
        return grEventsEntry;
    }

    @SuppressWarnings(value = {"unchecked"})
    private void createGrEventsEntry(MOFactory moFactory) {
        // Index definition
        grEventsEntryIndexes =
                new MOTableSubIndex[] {
                    moFactory.createSubIndex(
                            oidGrEventsEntryIndex, SMIConstants.SYNTAX_INTEGER, 1, 1)
                };

        grEventsEntryIndex =
                moFactory.createIndex(
                        grEventsEntryIndexes,
                        false,
                        new MOTableIndexValidator() {
                            public boolean isValidIndex(OID index) {
                                boolean isValidIndex = true;
                                // --AgentGen BEGIN=grEventsEntry::isValidIndex
                                // --AgentGen END
                                return isValidIndex;
                            }
                        });

        // Columns
        MOColumn<?>[] grEventsEntryColumns = new MOColumn<?>[4];
        grEventsEntryColumns[idxGrEventsEntryIndex] =
                moFactory.createColumn(
                        colGrEventsEntryIndex,
                        SMIConstants.SYNTAX_INTEGER,
                        moFactory.createAccess(MOAccessImpl.ACCESSIBLE_FOR_READ_ONLY));
        grEventsEntryColumns[idxGrEventsEntryDescription] =
                moFactory.createColumn(
                        colGrEventsEntryDescription,
                        SMIConstants.SYNTAX_OCTET_STRING,
                        moFactory.createAccess(MOAccessImpl.ACCESSIBLE_FOR_READ_ONLY));
        grEventsEntryColumns[idxGrEventsEntryTimeto] =
                moFactory.createColumn(
                        colGrEventsEntryTimeto,
                        SMIConstants.SYNTAX_OCTET_STRING,
                        moFactory.createAccess(MOAccessImpl.ACCESSIBLE_FOR_READ_ONLY));
        grEventsEntryColumns[idxGrEventsEntryStatus] =
                moFactory.createColumn(
                        colGrEventsEntryStatus,
                        SMIConstants.SYNTAX_OCTET_STRING,
                        moFactory.createAccess(MOAccessImpl.ACCESSIBLE_FOR_READ_ONLY));
        // Table model
        grEventsEntryModel =
                moFactory.createTableModel(
                        oidGrEventsEntry, grEventsEntryIndex, grEventsEntryColumns);
        ((MOMutableTableModel<GrEventsEntryRow>) grEventsEntryModel)
                .setRowFactory(new GrEventsEntryRowFactory());
        grEventsEntry =
                moFactory.createTable(
                        oidGrEventsEntry,
                        grEventsEntryIndex,
                        grEventsEntryColumns,
                        grEventsEntryModel);
    }

    public void registerMOs(MOServer server, OctetString context)
            throws DuplicateRegistrationException {
        // Scalar Objects
        server.register(this.grEventsNumber, context);
        server.register(this.grEventsEntry, context);
        // --AgentGen BEGIN=_registerMOs
        // --AgentGen END
    }

    public void unregisterMOs(MOServer server, OctetString context) {
        // Scalar Objects
        server.unregister(this.grEventsNumber, context);
        server.unregister(this.grEventsEntry, context);
        // --AgentGen BEGIN=_unregisterMOs
        // --AgentGen END
    }

    // Notifications

    // Scalars

    // Value Validators

    // Rows and Factories

    public class GrEventsEntryRow extends DefaultMOMutableRow2PC {

        // --AgentGen BEGIN=grEventsEntry::RowMembers
        // --AgentGen END

        public GrEventsEntryRow(OID index, Variable[] values) {
            super(index, values);
            // --AgentGen BEGIN=grEventsEntry::RowConstructor
            // --AgentGen END
        }

        public Integer32 getGrEventsEntryIndex() {
            // --AgentGen BEGIN=grEventsEntry::getGrEventsEntryIndex
            // --AgentGen END
            return (Integer32) super.getValue(idxGrEventsEntryIndex);
        }

        public void setGrEventsEntryIndex(Integer32 newColValue) {
            // --AgentGen BEGIN=grEventsEntry::setGrEventsEntryIndex
            // --AgentGen END
            super.setValue(idxGrEventsEntryIndex, newColValue);
        }

        public OctetString getGrEventsEntryDescription() {
            // --AgentGen BEGIN=grEventsEntry::getGrEventsEntryDescription
            // --AgentGen END
            return (OctetString) super.getValue(idxGrEventsEntryDescription);
        }

        public void setGrEventsEntryDescription(OctetString newColValue) {
            // --AgentGen BEGIN=grEventsEntry::setGrEventsEntryDescription
            // --AgentGen END
            super.setValue(idxGrEventsEntryDescription, newColValue);
        }

        public OctetString getGrEventsEntryTimeto() {
            // --AgentGen BEGIN=grEventsEntry::getGrEventsEntryTimeto
            // --AgentGen END
            return (OctetString) super.getValue(idxGrEventsEntryTimeto);
        }

        public void setGrEventsEntryTimeto(OctetString newColValue) {
            // --AgentGen BEGIN=grEventsEntry::setGrEventsEntryTimeto
            // --AgentGen END
            super.setValue(idxGrEventsEntryTimeto, newColValue);
        }

        public OctetString getGrEventsEntryStatus() {
            // --AgentGen BEGIN=grEventsEntry::getGrEventsEntryStatus
            // --AgentGen END
            return (OctetString) super.getValue(idxGrEventsEntryStatus);
        }

        public void setGrEventsEntryStatus(OctetString newColValue) {
            // --AgentGen BEGIN=grEventsEntry::setGrEventsEntryStatus
            // --AgentGen END
            super.setValue(idxGrEventsEntryStatus, newColValue);
        }

        public Variable getValue(int column) {
            // --AgentGen BEGIN=grEventsEntry::RowGetValue
            // --AgentGen END
            switch (column) {
                case idxGrEventsEntryIndex:
                    return getGrEventsEntryIndex();
                case idxGrEventsEntryDescription:
                    return getGrEventsEntryDescription();
                case idxGrEventsEntryTimeto:
                    return getGrEventsEntryTimeto();
                case idxGrEventsEntryStatus:
                    return getGrEventsEntryStatus();
                default:
                    return super.getValue(column);
            }
        }

        public void setValue(int column, Variable value) {
            // --AgentGen BEGIN=grEventsEntry::RowSetValue
            // --AgentGen END
            switch (column) {
                case idxGrEventsEntryIndex:
                    setGrEventsEntryIndex((Integer32) value);
                    break;
                case idxGrEventsEntryDescription:
                    setGrEventsEntryDescription((OctetString) value);
                    break;
                case idxGrEventsEntryTimeto:
                    setGrEventsEntryTimeto((OctetString) value);
                    break;
                case idxGrEventsEntryStatus:
                    setGrEventsEntryStatus((OctetString) value);
                    break;
                default:
                    super.setValue(column, value);
            }
        }

        // --AgentGen BEGIN=grEventsEntry::Row
        // --AgentGen END
    }

    public class GrEventsEntryRowFactory implements MOTableRowFactory<GrEventsEntryRow> {
        public synchronized GrEventsEntryRow createRow(OID index, Variable[] values)
                throws UnsupportedOperationException {
            GrEventsEntryRow row = new GrEventsEntryRow(index, values);
            // --AgentGen BEGIN=grEventsEntry::createRow
            // --AgentGen END
            return row;
        }

        public synchronized void freeRow(GrEventsEntryRow row) {
            // --AgentGen BEGIN=grEventsEntry::freeRow
            // --AgentGen END
        }

        // --AgentGen BEGIN=grEventsEntry::RowFactory
        // --AgentGen END
    }

    // --AgentGen BEGIN=_METHODS
    // --AgentGen END

    // Textual Definitions of MIB module GrEventsSnmpMib
    protected void addTCsToFactory(MOFactory moFactory) {}

    // --AgentGen BEGIN=_TC_CLASSES_IMPORTED_MODULES_BEGIN
    // --AgentGen END

    // Textual Definitions of other MIB modules
    public void addImportedTCsToFactory(MOFactory moFactory) {}

    // --AgentGen BEGIN=_TC_CLASSES_IMPORTED_MODULES_END
    // --AgentGen END

    // --AgentGen BEGIN=_CLASSES
    // --AgentGen END

    // --AgentGen BEGIN=_END
    // --AgentGen END
}
