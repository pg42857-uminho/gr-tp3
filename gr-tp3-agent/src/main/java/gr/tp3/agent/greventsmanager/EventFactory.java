package gr.tp3.agent.greventsmanager;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class EventFactory {

    public static final String DESCRIPTION_PROPERTY = "description";
    public static final String PASTPHRASE_PROPERTY = "past_phrase";
    public static final String PRESENTPHRASE_PROPERTY = "present_phrase";
    public static final String FUTUREPHRASE_PROPERTY = "future_phrase";
    public static final String DURATION_PROPERTY = "duration";
    public static final String INITIALDATE_PROPERTY = "initial_date";
    public static final String DELETEAFTER_PROPERTY = "delete_after";
    public static final String EVENTS_PROPERTY = "events";

    protected List<Event> createEvents(JSONObject jsonObject){
        List<Event> eventList = new ArrayList<>();

        try {
            JSONArray jsonEventsArray = (JSONArray) jsonObject.get(EVENTS_PROPERTY);

            for (Object object : jsonEventsArray) {
                JSONObject jsonEventObject = (JSONObject) object;

                Optional<Event> eventOptional = createEvent(jsonEventObject);

                if (eventOptional.isPresent()) {
                    Event event = eventOptional.get();
                    eventList.add(event);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return eventList;
    }

    private Optional<Event> createEvent(JSONObject jsonEvent) {
        try {
            String description = (String) jsonEvent.get(DESCRIPTION_PROPERTY);
            String pastPhrase = (String) jsonEvent.get(PASTPHRASE_PROPERTY);
            String presentPhrase = (String) jsonEvent.get(PRESENTPHRASE_PROPERTY);
            String futurePhrase = (String) jsonEvent.get(FUTUREPHRASE_PROPERTY);
            long duration = (long) jsonEvent.get(DURATION_PROPERTY);
            String initialDate = (String) jsonEvent.get(INITIALDATE_PROPERTY);

            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = format.parse(initialDate);
            long initialDateTs = date.getTime();

            Event event;

            if (jsonEvent.containsKey(DELETEAFTER_PROPERTY)) {
                long deleteAfter = (long) jsonEvent.get(DELETEAFTER_PROPERTY);

                event = new Event(
                        description,
                        pastPhrase,
                        presentPhrase,
                        futurePhrase,
                        initialDateTs,
                        duration,
                        deleteAfter);
            }
            else {
                 event = new Event(
                        description,
                        pastPhrase,
                        presentPhrase,
                        futurePhrase,
                        initialDateTs,
                        duration);
            }
            
            return Optional.of(event);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }

}
