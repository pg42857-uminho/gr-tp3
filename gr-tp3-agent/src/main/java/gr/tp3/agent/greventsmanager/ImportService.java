package gr.tp3.agent.greventsmanager;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.util.List;

public class ImportService {
    private EventFactory theEventFactory;
    private String conf_file_alert = "./configuration.json";

    public ImportService() {
        theEventFactory = new EventFactory();
    }

    private JSONObject json_interpreter(String path) {
        JSONParser parser = new JSONParser();
        JSONObject theJSONObject = new JSONObject();
        try {
            Object obj = parser.parse(new FileReader(path));
            return (JSONObject) obj;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return theJSONObject;
    }

        public List<Event> listImporter() {
            JSONObject theNewImport = json_interpreter(conf_file_alert);
            return theEventFactory.createEvents(theNewImport);
        }
}
