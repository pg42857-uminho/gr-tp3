package gr.tp3.agent.greventsmanager;

import gr.tp3.agent.GrEventsSnmpMib;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.Variable;

import java.util.*;

public class GrEventsSnmpMibManager extends Thread {

    private static int counter = 0;

    private Map<String, Integer> indexToDescriptionMap;
    private Map<String, Event> eventMap;

    private ImportService theImportService;
    private GrEventsSnmpMib grEventsSnmpMib;

    public GrEventsSnmpMibManager(GrEventsSnmpMib grEventsSnmpMib) {
        this.grEventsSnmpMib = grEventsSnmpMib;
        theImportService = new ImportService();
        eventMap = new HashMap<>();
        indexToDescriptionMap = new HashMap<>();
    }

    @Override
    public void run() {
        while (true) {

            List<Event> eventList = theImportService.listImporter();
            updateEvents(eventList);

            try {
                Thread.sleep(60 * 1000); // 60 second refresh rate
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void updateEvents(List<Event> eventList) {
        Map<String, Event> previousEvents = new HashMap<>(eventMap);
        eventMap.clear();
        for (Event event : eventList) {
            String description = event.getDescription();

            if (!event.isToBeDeleted() && !eventMap.containsKey(description)) {
                //If it does not contain the event => Create
                if (!previousEvents.containsKey(description)) {
                    eventMap.put(description, event);

                    System.out.printf("Created new event : %s\n", description);
                    createMIBEntry(event);
                }
                else {
                    Event previousEvent = previousEvents.get(description);
                    eventMap.put(description, event);

                    if (!previousEvent.equals(event)) {
                        System.out.printf("Updated event : %s\n", description);
                    }
                    else {
                        System.out.printf("Updated event time to : %s\n", description);
                    }

                    updateMIBEntry(event);
                }
            }
        }

        for (Map.Entry<String, Event> previousEventEntry : previousEvents.entrySet()) {
            String description = previousEventEntry.getKey();
            Event event = previousEventEntry.getValue();

            //If one of the previous events does not exist already => Delete
            if (!eventMap.containsKey(description)) {
                System.out.printf("Deleted event : %s\n", description);
                deleteMIBEntry(event);
            }
        }

        updateMIBNumber(this.eventMap.size());
    }

    private void createMIBEntry(Event event) {
        String description = event.getDescription();

        int index = counter++;
        indexToDescriptionMap.put(description, index);
        
        updateMIBEntry(event);
    }

    private void updateMIBEntry(Event event) {
        String description = event.getDescription();
        int index = indexToDescriptionMap.get(description);
        String timeToString = event.getTimeToString();
        String status = event.getStatus();

        Variable[] variables = new Variable[4];
        variables[GrEventsSnmpMib.idxGrEventsEntryIndex] = new Integer32(index);
        variables[GrEventsSnmpMib.idxGrEventsEntryDescription] = new OctetString(description);
        variables[GrEventsSnmpMib.idxGrEventsEntryTimeto] = new OctetString(timeToString);
        variables[GrEventsSnmpMib.idxGrEventsEntryStatus] = new OctetString(status);

        grEventsSnmpMib.getGrEventsEntry().addNewRow(new OID(new int[] {index}), variables);
    }

    private void deleteMIBEntry(Event event) {
        String description = event.getDescription();
        if (indexToDescriptionMap.containsKey(description)) {
            int index = indexToDescriptionMap.get(description);

            grEventsSnmpMib.getGrEventsEntry().removeRow(new OID(new int[] {index}));
            indexToDescriptionMap.remove(description);
        }
    }

    private void updateMIBNumber(int number) {
        Integer32 value = new Integer32(number);
        grEventsSnmpMib.getGrEventsNumber().setValue(value);
    }

}
