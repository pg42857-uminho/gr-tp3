package gr.tp3.agent.greventsmanager;

import java.time.*;
import java.util.Objects;

public class Event {

    private String description;
    private String pastPhrase;
    private String presentPhrase;
    private String futurePhrase;
    private long initialDate;
    private long duration;
    private long deleteAfter;

    protected Event(String description,
                    String pastPhrase,
                    String presentPhrase,
                    String futurePhrase,
                    long initialDate,
                    long duration) {
        this(description, pastPhrase, presentPhrase, futurePhrase, initialDate, duration, -1);
    }

    protected Event(String description,
                    String pastPhrase,
                    String presentPhrase,
                    String futurePhrase,
                    long initialDate,
                    long duration,
                    long deleteAfter) {
        setDescription(description);
        setPastPhrase(pastPhrase);
        setPresentPhrase(presentPhrase);
        setFuturePhrase(futurePhrase);
        setInitialDate(initialDate);
        setDuration(duration);
        setDeleteAfter(deleteAfter);
    }

    public long getTimeTo() {
        long now = System.currentTimeMillis();
        if (now < initialDate) {
            return initialDate - now;
        }
        else if (now < initialDate + duration) {
            return 0;
        }
        else {
            return (initialDate + duration) - now;
        }
    }

    public String getTimeToString() {
        long timeTo = getTimeTo();

        if (timeTo == 0) {
            return String.format("+00 years +00 months +00 weeks +00 days +00 hours +00 minutes");
        }
        else {
            Instant instant;

            if (timeTo > 0) {
                instant = Instant.ofEpochMilli(initialDate);
            }
            else {
                instant = Instant.ofEpochMilli(initialDate + duration);
            }

            LocalDateTime date = instant.atZone(ZoneId.systemDefault()).toLocalDateTime();
            LocalDateTime now = LocalDateTime.now();

            long years = Math.abs(Period.between(now.toLocalDate(), date.toLocalDate()).getYears());
            long months = Math.abs(Period.between(now.toLocalDate(), date.toLocalDate()).getMonths());
            long daysTotal = Math.abs(Duration.between(now, date).toDays());

            long weeks = daysTotal / 7;
            long days = daysTotal % 7;

            long hours = Math.abs(Duration.between(now, date).toHoursPart());
            long minutes = Math.abs(Duration.between(now, date).toMinutesPart());

            if (timeTo > 0) {
                return String.format("+%02d years +%02d months +%02d weeks +%02d days +%02d hours +%02d minutes",
                        years, months, weeks, days, hours, minutes);
            }
            else {
                return String.format("-%02d years -%02d months -%02d weeks -%02d days -%02d hours -%02d minutes",
                        years, months, weeks, days, hours, minutes);
            }
        }
    }


    public String getStatus() {
        long timeTo = getTimeTo();

        if (timeTo < 0) {
            return pastPhrase;
        } else if (timeTo > 0) {
            return futurePhrase;
        } else {
            return presentPhrase;
        }
    }

    public boolean isToBeDeleted() {
        if (deleteAfter < 0) {
            return false;
        }

        long millisDeleteAfter = deleteAfter * 24L * 60L * 60L * 1000L;
        boolean isToBeDeleted = (initialDate + duration + millisDeleteAfter) < System.currentTimeMillis();

        return isToBeDeleted;
    }

    private void setDescription(String description) {
        if (description == null || description.trim().isEmpty()) {
            throw new IllegalArgumentException("Description can't be null nor empty!");
        }

        this.description = description;
    }

    private void setPastPhrase(String pastPhrase) {
        if (pastPhrase == null || pastPhrase.trim().isEmpty()) {
            throw new IllegalArgumentException("Past phrase can't be null nor empty!");
        }

        this.pastPhrase = pastPhrase;
    }

    private void setPresentPhrase(String presentPhrase) {
        if (presentPhrase == null || presentPhrase.trim().isEmpty()) {
            throw new IllegalArgumentException("Present phrase can't be null nor empty!");
        }

        this.presentPhrase = presentPhrase;
    }

    private void setFuturePhrase(String futurePhrase) {
        if (futurePhrase == null || futurePhrase.trim().isEmpty()) {
            throw new IllegalArgumentException("Future phrase can't be null nor empty!");
        }

        this.futurePhrase = futurePhrase;
    }

    private void setInitialDate(long initialDate) {
        if (initialDate < 0) {
            throw new IllegalArgumentException("Initial date can't be a negative value (milliseconds since Epoch)!");
        }

        this.initialDate = initialDate;
    }

    private void setDuration(long duration) {
        if (duration < 0) {
            throw new IllegalArgumentException("Duration can't be a negative value!");
        }

        this.duration = duration*60*1000;
    }

    private void setDeleteAfter(long deleteAfter) {
        this.deleteAfter = deleteAfter;
    }

    public String getDescription() {
        return description;
    }

    public String getPastPhrase() {
        return pastPhrase;
    }

    public String getPresentPhrase() {
        return presentPhrase;
    }

    public String getFuturePhrase() {
        return futurePhrase;
    }

    public long getInitialDate() {
        return initialDate;
    }

    public long getDuration() {
        return duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return initialDate == event.initialDate &&
                duration == event.duration &&
                Objects.equals(description, event.description) &&
                Objects.equals(pastPhrase, event.pastPhrase) &&
                Objects.equals(presentPhrase, event.presentPhrase) &&
                Objects.equals(futurePhrase, event.futurePhrase);
    }


    @Override
    public int hashCode() {
        return Objects.hash(description, pastPhrase, presentPhrase, futurePhrase, initialDate, duration);
    }

    @Override
    public String toString() {
        return "Event{" +
                "description='" + description + '\'' +
                ", pastPhrase='" + pastPhrase + '\'' +
                ", presentPhrase='" + presentPhrase + '\'' +
                ", futurePhrase='" + futurePhrase + '\'' +
                ", initialDate=" + initialDate +
                ", duration=" + duration +
                '}';
    }
}
